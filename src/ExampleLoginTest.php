<?php

// Use your module's testing namespace such as the one below.

namespace Drupal\Tests\moduleName\ExistingSite;

use weitzman\DrupalTestTraits\ExistingSiteBase;
use weitzman\LoginTrait\LoginTrait;

/**
 * Login and Logout via user reset URL instead of forms. Useful when TFA/SAML are enabled.
 */
class ExampleLoginTest extends ExistingSiteBase
{
    use LoginTrait;

    /**
     * Login and logout via password reset URL.
     */
    public function testLoginLogout()
    {
        // Creates a user. Will be automatically cleaned up at the end of the test.
        $user = $this->createUser();
        $this->drupalLogin($user);
        $this->drupalGet('user');
        $user2 = $this->createUser();
        $this->drupalLogin($user2);
    }
}
